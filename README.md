# Polkadot Scanner

## Prerequisite

- node: above version `v14.15.3`

## How to run

- `yarn && yarn start`

## Unit test

- `yarn && yarn test`

## Approach

Iterate input block number reversely, get block hash and get block information one a time, generate data then return to display on UI.

Used following endpoing from polkadot:
- `api.rpc.chain.getBlockHash`
- `api.rpc.chain.getBlock`
- `api.query.system.events.at`

The limit is this can query one block a time, and unfortunely I can see polkadot provide any range query api, looks like some range related api call marked as unsafe and throwing error now too.

**This make the query slow, so for quicker test, please provide smaller range of blocks.**

Another thing is I learnt that some RPC are not supported by default api promise initializing, because they have their own metadata types.

**Please use RPCs that fully match with latest api's default metadata types.**

## Afterword

I found it's quite hard to read the docs of polkadot.js, spend more time than I expected to find api to fullfill the feature.

Not sure if my approach is 100% the best approach, but willing to learn more if there are better api to use.
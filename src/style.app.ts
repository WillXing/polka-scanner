import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    root: {
        height: '100%',
        textAlign: 'center'
    },
    container: {
        height: '100%'
    },
    title: {
        marginTop: 20,
        '& img': {
           height: 96
        }
    },
    fields: {
        margin: '20px 0 20px 0'
    },
    eventTable: {
        display: 'flex',
        flexGrow: 0.9
    },
    '@media screen and (max-width: 600px)': {
        title: {
            '& h2': {
                fontSize: '70px',
            },
            '& img': {
                height: 70
            }
        },
    }
}));

export default useStyles
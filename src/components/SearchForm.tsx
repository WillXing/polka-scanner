import {Button, TextField, Tooltip} from "@material-ui/core";
import React, {useState} from "react";
import useValidateFiled from "../hooks/useValidateField";
import useStyles from "./style.searchForm";

const SearchForm = ({scan, loading}: SearchFormProps) => {

    const classes = useStyles()

    const [startBlock, setStartBlock] = useState('1')
    const [endBlock, setEndBlock] = useState('2')
    const [endpoint, setEndpoint] = useState('wss://rpc.polkadot.io')
    const [startBlockError, endBlockError, endpointError] = useValidateFiled(startBlock, endBlock, endpoint)

    return (
        <React.Fragment>
            <Tooltip title="Input a number greater than 0 and smaller than end block">
                <TextField
                    data-testid="start-block"
                    className={classes.inputField}
                    error={startBlockError}
                    helperText={startBlockError && "Invalid input"}
                    type="number" label="Start Block" variant="outlined"
                    value={startBlock} onChange={e => setStartBlock(e.target.value)}/>
            </Tooltip>
            <Tooltip title="Scan can took a while, please play with small range">
                <TextField
                data-testid="end-block"
                className={classes.inputField}
                error={endBlockError}
                helperText={endBlockError && "Invalid input"}
                type="number" label="End Block" variant="outlined"
                value={endBlock} onChange={e => setEndBlock(e.target.value)}/>
            </Tooltip>
            <Tooltip title="RPC with customized meta not supported">
                <TextField
                data-testid="endpoint"
                className={classes.inputField}
                error={endpointError}
                helperText={endpointError && "Wrong RPC format"}
                label="Endpoint" variant="outlined"
                value={endpoint} onChange={e => setEndpoint(e.target.value)}
                placeholder='wss://rpc.polkadot.io'/>
            </Tooltip>
            <Button
                className={classes.scanButton}
                data-testid="scan-btn"
                disabled={startBlockError || endBlockError || endpointError || loading} variant="contained" color="primary" onClick={() => scan(startBlock, endBlock, endpoint || undefined)}>
                Scan
            </Button>
        </React.Fragment>
    )
}

interface SearchFormProps {
    scan: (startBlock: string, endBlock: string, endpoint?: string)  => void,
    loading: boolean
}

export default SearchForm
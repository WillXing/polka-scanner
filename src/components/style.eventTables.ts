import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    root: {
        margin: '0 20px',
        height: '100%',
        width: '100%'
    }
}));

export default useStyles
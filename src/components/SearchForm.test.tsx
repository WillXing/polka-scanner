import {render, fireEvent} from "@testing-library/react";
import SearchForm from "./SearchForm";

describe('SearchForm', () => {
    it('should disable scan button when endpoint is invalid', () => {
        const {getAllByRole, getByRole} = render(<SearchForm scan={jest.fn()} loading={false}/>);
        const endpoint = getAllByRole('textbox')[0];
        fireEvent.change(endpoint, {target: {value: '://123'}})
        expect(getByRole('button').className).toContain('Mui-disabled')
    })
    it('should disable scan button when start block minor than 0', () => {
        const {getAllByRole, getByRole} = render(<SearchForm scan={jest.fn()} loading={false}/>);
        const startBlock = getAllByRole('spinbutton')[0];
        fireEvent.change(startBlock, {target: {value: '-1'}})
        expect(getByRole('button').className).toContain('Mui-disabled')
    })
    it('should disable scan button when end block minor than start block', () => {
        const {getAllByRole, getByRole} = render(<SearchForm scan={jest.fn()} loading={false}/>);
        const startBlock = getAllByRole('spinbutton')[0];
        const endBlock = getAllByRole('spinbutton')[1];
        fireEvent.change(startBlock, {target: {value: '5'}})
        fireEvent.change(endBlock, {target: {value: '3'}})
        expect(getByRole('button').className).toContain('Mui-disabled')
    })
    it('should disable scan button when loading', () => {
        const {getByRole} = render(<SearchForm scan={jest.fn()} loading={true}/>);
        expect(getByRole('button').className).toContain('Mui-disabled')
    })
    it('should enable scan button when input are all valid', () => {
        const {getAllByRole, getByRole} = render(<SearchForm scan={jest.fn()} loading={false}/>);
        const startBlock = getAllByRole('spinbutton')[0];
        const endBlock = getAllByRole('spinbutton')[1];
        const endpoint = getAllByRole('textbox')[0];
        fireEvent.change(endpoint, {target: {value: 'wss://1.2.3'}})
        fireEvent.change(startBlock, {target: {value: '1'}})
        fireEvent.change(endBlock, {target: {value: '3'}})
        expect(getByRole('button').className).not.toContain('Mui-disabled')
    })
})
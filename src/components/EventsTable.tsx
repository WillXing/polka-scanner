import React from "react";
import {DataGrid} from "@material-ui/data-grid";
import useTableData from "../hooks/useTableData";
import useStyles from "./style.eventTables";

const EventsTable = ({blocksData, loading}: EventsTableProps) => {

    const classes = useStyles();

    const tableData = useTableData(blocksData);

    const columns = [
        {field: 'id', hide: true},
        {field: 'blockNumber', headerName: 'Block Number', flex: 0.2},
        {field: 'eventName', headerName: 'Event', flex: 0.2},
        {field: 'arguments', headerName: 'Arguments', flex: 1}
    ];

    return (
        <div className={classes.root}>
            <DataGrid loading={loading} rows={tableData} columns={columns} pageSize={20} rowHeight={50}/>
        </div>
    )
}

interface EventsTableProps {
    blocksData: BlockDetail[],
    loading: boolean
}

export default EventsTable;
import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
    inputField: {
        margin: '0 5px 0 0',
        width: 200,
        height: 56
    },
    scanButton: {
        width: 200,
        height: 56
    },
    '@media screen and (max-width: 600px)': {
        inputField: {
            display:'block',
            width: 100,
            margin: '2px 5px 20px 0',
        }
    }
}));

export default useStyles
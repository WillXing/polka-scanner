import React, {useCallback, useState} from 'react';
import {Grid, Typography} from "@material-ui/core";
import {Alert} from '@material-ui/lab';
import Logo from './assets/polkadot_logo.svg'
import {scanBlock} from "./services/blocksScanner";
import EventsTable from "./components/EventsTable";
import SearchForm from "./components/SearchForm";
import useStyles from "./style.app";

const App = () => {
    const classes = useStyles()
    const [loading, setLoading] = useState(false)
    const [blocksData, setBlocksData] = useState<BlockDetail[]>([])
    const [error, setError] = useState<Error | void>(undefined)
    const scan = useCallback((startBlock: string, endBlock: string, endpoint?: string) => {
        setLoading(true)
        setError(undefined)
        setBlocksData([])
        scanBlock(parseInt(startBlock), parseInt(endBlock), endpoint).then(blocks => {
            setBlocksData(blocks)
        }).catch(e => {
            setError(e)
        }).finally(() => {
            setLoading(false)
        })
    }, [])

    return (
        <div className={classes.root}>
            <Grid className={classes.container} container justify="flex-start" direction="column">
                <Grid className={classes.title} container justify="center">
                    <Typography variant="h1" component="h2">
                        <img src={Logo} alt=""/> scanner
                    </Typography>
                </Grid>
                {error && <Grid container justify="center">
                  <Grid item xs={8}>
                    <Alert severity="error">
                        {error?.message}
                    </Alert>
                  </Grid>
                </Grid>}
                <Grid className={classes.fields} container justify="center">
                    <SearchForm scan={scan} loading={loading}/>
                </Grid>
                <Grid className={classes.eventTable} container justify="center">
                    <EventsTable loading={loading} blocksData={blocksData}/>
                </Grid>
            </Grid>
        </div>
    );
};

export default App;

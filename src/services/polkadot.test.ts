import * as endpointValidator from '../utils/endpointValidator'
import * as pokadotService from "./polkadot";
jest.mock('@polkadot/api')

describe('Polkadot service', () => {
    it('should throw endpoint invalid error when fail the validate', () => {
        jest.spyOn(endpointValidator, 'validate').mockReturnValueOnce(false)
        expect(() => pokadotService.verifyEndpoint('wrong.url')).toThrow('Endpoint invalid')
    });

    it('should use polkadot as default endpoint when endpoint is undefined', () => {
        const endpoint = pokadotService.finalizeEndpoint()
        expect(endpoint).toBe('wss://rpc.polkadot.io')
    })
})
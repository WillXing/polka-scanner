import * as blocksScanner from "./blocksScanner"
import * as pokadotService from "./polkadot";

describe('Blocks scanner', () => {
    afterEach(() => {
        jest.clearAllMocks()
    })
    it('should iterate from startblock to endblock when scan blocks', async () => {
        jest.spyOn(pokadotService, 'fetchBlockDetail').mockResolvedValue({blockNumber: 0, events: []})
        const result = await blocksScanner.scanBlock(1,3)
        expect(pokadotService.fetchBlockDetail).toHaveBeenCalledTimes(3)
        expect(result.length).toBe(3)
    })
})
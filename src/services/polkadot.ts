import {ApiPromise, WsProvider} from "@polkadot/api";
import {validate} from "../utils/endpointValidator";
import {BlockHash} from "@polkadot/types/interfaces/chain/types";
import {EventRecord} from "@polkadot/types/interfaces/system";
import {GenericExtrinsic} from "@polkadot/types/extrinsic/Extrinsic";

let wsProvider: WsProvider | void;
let api: ApiPromise | void;
let currentEndpoint: string | void;

export const verifyEndpoint = (endpoint?: string) => {
    if (endpoint && !validate(endpoint)) {
        throw new Error('Endpoint invalid')
    }
}

export const finalizeEndpoint = (endpoint?: string) => {
    return endpoint || 'wss://rpc.polkadot.io'
}

export const setupWsProvider  = async (endpoint?: string ) : Promise<void> => {
    verifyEndpoint(endpoint);
    const newEndpoint = finalizeEndpoint(endpoint);
    if (newEndpoint === currentEndpoint) {
        return;
    } else {
        currentEndpoint = newEndpoint
        if (api) {await api.disconnect()}
    }
    wsProvider = new WsProvider(currentEndpoint);
    api = new ApiPromise({ provider: wsProvider });
    try {
        await api.isReadyOrError
    } catch (e) {
        await api.disconnect()
        api = undefined;
        wsProvider = undefined;
        throw new Error('This RPC is either not supported or timing out, try some other RPC endpoints. eg, wss://rpc.polkadot.io, wss://kusama-rpc.polkadot.io')
    }
}

export const fetchBlockDetail = async (blockNumber: number, endpoint?: string) : Promise<BlockDetail> => {
    await setupWsProvider(endpoint)
    if (!api) {throw new Error('Api promise initialize failed')}
    let signedBlock
    try {
        let blockHash: BlockHash = await api.rpc.chain.getBlockHash(blockNumber);
        signedBlock = await api.rpc.chain.getBlock(blockHash);
    } catch (e) {
        if (e.message === 'createType(BlockHash):: Cannot read property \'subarray\' of null') {
            throw new Error('End block not exists, lower the block number and try again.')
        } else {
            throw e
        }
    }

    const allRecords = await api.query.system.events.at(signedBlock.block.header.hash);
    const events: EventDetail[] = generateBlockEventInfo(allRecords, signedBlock.block.extrinsics)
    return {
        blockNumber,
        events
    }
}


export const generateBlockEventInfo = (eventsRecords: EventRecord[], blockExtrinsics: GenericExtrinsic[]): EventDetail[] => {
    const events: EventDetail[] = []
    blockExtrinsics.forEach((ex, index) => {
        const eventsForEx = eventsRecords
            .filter(({phase}) =>
                phase.isApplyExtrinsic &&
                phase.asApplyExtrinsic.eq(index)
            )
            .map(({event }) => {
                return ({
                    hash: event.data.hash.toHuman() as string,
                    name: `${event.section}.${event.method}`,
                    arguments: event.data.meta.args.toHuman() as string[],
                    argumentsValue: (event.data.toHuman() as any[]).map(value => JSON.stringify(value))
                });
            });
        events.push(...eventsForEx)
    });
    return events;
}
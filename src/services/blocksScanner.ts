import {fetchBlockDetail} from "./polkadot";

export const scanBlock = async (startBlock: number, endBlock: number, endpoint?: string) : Promise<BlockDetail[]>  => {
  const result: BlockDetail[] = [];
  for (let blockNumber = endBlock; blockNumber >= startBlock; blockNumber--) {
    const block = await fetchBlockDetail(blockNumber, endpoint)
    result.push(block)
  }
  return result
}

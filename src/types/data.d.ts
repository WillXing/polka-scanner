interface BlockDetail {
    blockNumber: number,
    events: EventDetail[]
}

interface EventDetail {
    hash: string,
    name: string,
    arguments: string[],
    argumentsValue: string[]
}

interface TableBlockEventData {
    id: string,
    blockNumber: number,
    eventName: string,
    arguments: string
}
import {useEffect, useState} from "react";

const useTableData = (blocks: BlockDetail[]) => {
    const [tableData, setTableData] = useState<TableBlockEventData[]>([])

    useEffect(() => {
        const blockEvents: TableBlockEventData[] = [];
        blocks.forEach(block => {
            const events = block.events.map(event => ({
                id: `${block.blockNumber}-${event.hash}`,
                blockNumber: block.blockNumber,
                eventName: event.name,
                arguments: argumentStringify(event)
            }));

            blockEvents.push(...events)
        })
        setTableData(blockEvents);
    }, [blocks])

    return tableData
}

export const argumentStringify = (event: EventDetail) => {
    return event.argumentsValue.map((value, index) => `${event.arguments[index]}=${value}`).join(', ')
}

export default useTableData
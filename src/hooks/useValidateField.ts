import {useEffect, useState} from "react";
import {validate} from "../utils/endpointValidator";

const useValidateFiled = (startBlock: string, endBlock: string, endpoint: string) => {
    const [startBlockError, setStartBlockError] = useState(false)
    const [endBlockError, setEndBlockError] = useState(false)
    const [endpointError, setEndpointError] = useState(false)

    useEffect(() => {
        setStartBlockError(startBlock === '' || endBlock < startBlock || parseInt(startBlock) <= 0)
        setEndBlockError(endBlock === '' || endBlock < startBlock || parseInt(endBlock) <= 0)
        setEndpointError(endpoint !== '' && !validate(endpoint))
    }, [startBlock, endBlock, endpoint])

    return [startBlockError, endBlockError, endpointError]
}

export default useValidateFiled
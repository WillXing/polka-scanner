import {argumentStringify} from "./useTableData";

describe('useTableData', () => {
    it('argument stringify should concat arguments into single string', () => {
        const event: EventDetail = {
            hash: '',
            name: '',
            arguments: ['a1', 'a2'],
            argumentsValue: ['v1', 'v2']
        }
        expect(argumentStringify(event)).toEqual('a1=v1, a2=v2')
    })
})
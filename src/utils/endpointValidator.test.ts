import {validate} from "./endpointValidator";

describe('Endpoint validator', () => {
    it('should return true when endpoint is websocket url', function () {
        const wsUrl = 'wss://pc.polkadot.io'
        expect(validate(wsUrl)).toBeTruthy()
    });
    it('should return false when endpoint is not websocket url', function () {
        const wsUrl = 'https://pc.polkadot.io'
        expect(validate(wsUrl)).toBeFalsy()
    });
})
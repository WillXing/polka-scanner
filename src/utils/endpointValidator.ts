const validate = (endpoint: string) : boolean => {
    const wsRegex = /^(wss?):\/\/[^\s$.?#].[^\s]*$/
    return wsRegex.test(endpoint)
}

export {validate}